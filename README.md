# OpenID Connect Azure B2C

A module that simplifies the connection of Azure AD to Drupal.

## Changes

### 1.0.1

- Declare support for Drupal 10 and openid_connect 3

### 1.0.0

- Initial public release
